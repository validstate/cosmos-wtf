from collections import Counter
import json
import pathlib

from flask import Flask, request
from flask import render_template
import requests
import toml

app = Flask(__name__)

ip_counter = Counter()
geo_lookup = {}

@app.route('/')
def index():
    data = json.load(open('data.json'))

    global ip_counter
    ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
    ip_counter[ip] += 1

    global geo_lookup
    try: 
        geo = geo_lookup.get(ip, requests.get(f'http://freegeoip.net/json/{ip}').json())
        country = geo['country_code']
        region = geo['region_name']
    except: 
        geo = country = region = 'N/a'
    geo_lookup[ip] = geo
    print(f'{ip} ({region}, {country})')
    print(ip_counter.most_common(10))
    return render_template('index.html', data=data)

