
CONDA_DIR = ./conda
CONDA = ${CONDA_DIR}/bin/conda
CONDA_PYTHON = ${CONDA_DIR}/bin/python
CONDA_INSTALLER = Miniconda3-latest-Linux-x86_64.sh

.PHONY: install-conda update-conda setup-conda clean-conda

update-conda:
	${CONDA} update -y conda
	${CONDA} env update -f environment.yml

setup-conda:  install-conda update-conda

install-conda:
	wget https://repo.continuum.io/miniconda/${CONDA_INSTALLER}
	bash ${CONDA_INSTALLER} -b -p ${CONDA_DIR}
	rm ${CONDA_INSTALLER}

clean-conda:
	rm -rf ${CONDA_DIR}

data-collection:
	while true; do conda/bin/python data-collector.py; sleep 10; done
dev:
	FLASK_APP=cosmos-wtf.py FLASK_DEBUG=1 conda/bin/flask run --host 0.0.0.0

prod:
	while true; do FLASK_APP=cosmos-wtf.py conda/bin/flask run; sleep 10; done
