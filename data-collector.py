import concurrent.futures
from datetime import datetime
import functools
import json
import pathlib
import time

import requests
import toml

def get_lbh(ep):
    try:
        r = requests.get(f'http://{ep}/abci_info', timeout=0.5)
    except requests.exceptions.ConnectTimeout:
        return "connection timeout"
    except requests.exceptions.ConnectionError as e:
        if 'Connection refused' in str(e):
            return "connection refused"
        else:
            return f"connection error: {str(e)}"
    except Exception as e:
        return f"exception connecting: {str(e)}"

    try:
        return r.json()['result']['response']['last_block_height']
    except Exception as e:
        return f"json error: {str(e)}"

def parallel_map(function, parameters):
    with concurrent.futures.ProcessPoolExecutor() as executor:
        responses = executor.map(function, parameters)
    return responses

def collect_data():
    net_paths = sorted(pathlib.Path('nets').glob('*'))
    now = datetime.utcnow().isoformat()[:19] + 'Z'
    data = {'last_collected': now}
    data_inner = data['statii'] = {}
    for p in net_paths:
        net = p.name
        if net not in ('gaia-3003',): # 'gaia-3004'):
            continue
        print(p)
        p2p = toml.load(str(p / 'config.toml'))['p2p']['seeds'].split(',')
        rpc = [ep[:-1] + '7'
               if ep != 'peer.gaia-3003.valid.st:5653'
               else 'call.gaia-3003.valid.st'
               for ep in p2p
              ]
        data_inner[net] = {
                ep: lbh for ep, lbh in zip(rpc, parallel_map(get_lbh, rpc))
        }
    print(now)

    return data


if '__main__' == __name__:
    while True:
        data = collect_data()
        json.dump(data, open('data.json', 'w'))
        time.sleep(30)

